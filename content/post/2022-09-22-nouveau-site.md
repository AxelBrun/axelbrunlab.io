---
title: Nouveau site de Rebecca et Axel
subtitle: Création de ce site 
date: 2022-09-22
author: Axel Brun
---

Ce site a été crée à l'aide du générateur [Hugo](https://gohugo.io/).

![logo hugo](/img/logohugo.png)

Il est herbergé sur [GitLab](https://gitlab.com/).


Ce post sert également de documentation personnelle pour gérer le contenu du site.


### Gérer le contenu du site

Les images sont a déposer dans /static/img 

Les posts sont à  créer sous la forme de fichier .md et à déposer sous /content/post

content/_index.md gère la fenètre d'accueil



début Exemple de post :

---
title: texte du titre
subtitle: texte du sous titre
date: YYYY-MM-JJ
author: Nom de l'auteur
---

Texte du blog

### Texte en format titre 3
- Element de liste 1
- Element de liste 2
- Element de liste 3

Faire un lien vers un autre post : 
[texte du lien](/post/YYYY-MM-JJ-exempledepost/)

Faire un lien vers une site web : 
[GitLab](https://gitlab.com/).


Faire une galerie d'image : 
{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="https://media.istockphoto.com/photos/sunset-at-savannah-plains-picture-id1140829787?k=20&m=1140829787&s=612x612&w=0&h=jn3PodUQjCQe3nqK6l0rEvR05uteoADuZNmdpCmFpA8=" caption="Afrique">}}
  {{< figure thumb="-thumb" link="https://static.wixstatic.com/media/108ff4_73d6e21164dd455699aab2d6b3afab8a~mv2.jpg/v1/fill/w_1200,h_630,al_c/108ff4_73d6e21164dd455699aab2d6b3afab8a~mv2.jpg" caption="Asie" >}}
  {{< figure thumb="-thumb" link="https://lesvolsdalexi.s3.ca-central-1.amazonaws.com/blog/20190528134021/cover-peru-1152x605.jpg" caption="Texte3">}}
{{< /gallery >}}

Inserer une image dans une page :  
![Example image](/img/exempleimage.png)

fin Exemple de post 


Pour déployer les changements fait au site :
	CI/CD -> Pipelines -> Run pipeline
