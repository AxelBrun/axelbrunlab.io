---
title: Notre itinéraire!
subtitle: Découvrez par où nous sommes passés, et où nous allons !
comments: false
---

L'itinéraire n'est pas encore établi, revenez plus tard.

![image itineraire](https://www.brilliant-africa.com/assets/front/dist/img/question-icon-map.svg)
